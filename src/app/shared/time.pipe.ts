import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
  name: 'time'
})
@Injectable({
  providedIn: 'root'
})
export class TimePipe implements PipeTransform {

  transform(value: number): string { // time in seconds to mm:ss
    if (value) {
      const intValue = value | 0;
      const minutes = (String)((intValue / 60) | 0);
      const seconds = (String)(intValue % 60);
      return `${minutes.padStart(2, '0')}:${seconds.padStart(2, '0')}`;
    }
    return null;
  }

}
