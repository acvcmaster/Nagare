import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { TimePipe } from '../time.pipe';
import { AudioInfo } from './audioinfo.model';

@Pipe({
  name: 'playtime'
})
@Injectable({
  providedIn: 'root'
})
export class PlaytimePipe implements PipeTransform {

  constructor(private timePipe: TimePipe) {  }

  transform(value: AudioInfo): string {
    if (value.progress && value.duration) {
      const current = this.timePipe.transform(value.progress * value.duration);
      const total = this.timePipe.transform(value.duration);
      return `${current} / ${total}`;
    }
    return null;
  }

}
