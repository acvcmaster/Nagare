import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaytimePipe } from './playtime/playtime.pipe';
import { TimePipe } from './time.pipe';
import { StarRatingComponent } from './star-rating/star-rating.component';



@NgModule({
  declarations: [PlaytimePipe, TimePipe, StarRatingComponent],
  imports: [
    CommonModule
  ],
  exports: [PlaytimePipe, TimePipe, StarRatingComponent]
})
export class SharedModule { }
