import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent {

  constructor() { }
  rating = '';
  val = 0;

  get value(): number {
    return this.val;
  }

  @Input() set value(val: number) {
    this.val = val;
    this.update();
  }

  update() {
    let result = '';
    if (this.value && (this.value > 0 && this.value <= 5)) {
      result = '';
      for (let i = 1; i <= 5; i++) {
        result += i <= this.value ? '★' : '☆';
      }
    }
    this.rating = result;
  }
}
