import { Component, OnInit, ViewChild, ElementRef, OnDestroy, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Playlist } from './playlist/playlist.model';
import { Subscription } from 'rxjs';
import { Song } from './song/song.model';
import { QueueService } from './queue/queue.service';
import { SearchService } from './search/search.service';
import { AudioService } from '../audio/audio.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit, OnDestroy {
  constructor(
    private readonly queueService: QueueService,
    public readonly searchService: SearchService,
    public readonly audioService: AudioService,
    private cd: ChangeDetectorRef
  ) { }

  subscriptions = new Subscription();
  selectedSong: Song;

  ngOnInit() {
    this.subscriptions.add(this.queueService.init().subscribe(success => success ? this.searchService.filter('') : null));
    this.subscriptions.add(this.queueService.songSubject.subscribe(song => this.selectedSong = song));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
