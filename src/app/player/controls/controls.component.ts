import { Component, OnInit } from '@angular/core';
import { QueueService } from '../queue/queue.service';
import { AudioService } from 'src/app/audio/audio.service';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.scss']
})
export class ControlsComponent implements OnInit {

  constructor(
    private readonly queueService: QueueService,
    private readonly audioService: AudioService
    ) { }

  ngOnInit() {
  }

  get playing() {
    return this.queueService.playing;
  }

  get shuffle() {
    return this.queueService.shuffled;
  }

  previousSong() {
    this.queueService.previousSong();
  }

  nextSong() {
    this.queueService.nextSong();
  }

  togglePlay() {
    this.queueService.playing = !this.queueService.playing;
  }

  toggleShuffle() {
    this.queueService.shuffled = !this.queueService.shuffled;
  }

  get volume() {
    return (100 * this.audioService.volume) | 0;
  }

  set volume(volume: number) {
    this.audioService.volume = volume / 100;
  }
}
