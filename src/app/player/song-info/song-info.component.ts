import { Component, OnInit, Input } from '@angular/core';
import { SearchService } from '../search/search.service';
import { Song } from '../song/song.model';

@Component({
  selector: 'app-song-info',
  templateUrl: './song-info.component.html',
  styleUrls: ['./song-info.component.scss']
})
export class SongInfoComponent implements OnInit {

  constructor(private readonly searchService: SearchService) { }

  @Input() song: Song;

  ngOnInit() {
  }

  filter(filter, type) {
    this.searchService.filter(filter, type);
  }
}
