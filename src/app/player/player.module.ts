import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerRoutingModule } from './player-routing.module';
import { PlayerComponent } from './player.component';
import { SearchComponent } from './search/search.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { TimebarComponent } from './timebar/timebar.component';
import { ControlsComponent } from './controls/controls.component';
import { SongListComponent } from './song-list/song-list.component';
import { AlbumCoverComponent } from './album-cover/album-cover.component';
import { SongInfoComponent } from './song-info/song-info.component';


@NgModule({
  declarations: [
    PlayerComponent,
    SearchComponent,
    TimebarComponent,
    ControlsComponent,
    SongListComponent,
    AlbumCoverComponent,
    SongInfoComponent
  ],
  imports: [
    CommonModule,
    PlayerRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class PlayerModule { }
