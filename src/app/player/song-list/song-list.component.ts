import { Component, OnInit } from '@angular/core';
import { Song } from '../song/song.model';
import { QueueService } from '../queue/queue.service';
import { SearchService } from '../search/search.service';

@Component({
  selector: 'app-song-list',
  templateUrl: './song-list.component.html',
  styleUrls: ['./song-list.component.scss']
})
export class SongListComponent {

  constructor(
    private readonly searchService: SearchService,
    private readonly queueService: QueueService
  ) { }

  selectedSong: Song;

  get filteredPlaylist(): Song[] {
    return this.searchService.filteredPlaylist;
  }

  isPlaying(song: Song): boolean {
    return this.queueService.isPlaying(song);
  }

  isSelected(song: Song): boolean {
    if (this.selectedSong) {
      return this.selectedSong.name === song.name;
    }
    return false;
  }

  select(song: Song) {
    if (this.queueService.currentSong.playlist_index !== song.playlist_index || !this.queueService.playing) {
      this.selectedSong = song;
      this.queueService.changeSong(song);
    }
  }

  previous(event) {
    if (event.target && event.target.previousSibling &&
      typeof (event.target.previousSibling.focus) === 'function') {
      event.target.previousSibling.focus();
    }
  }

  next(event) {
    if (event.target && event.target.nextSibling &&
      typeof (event.target.nextSibling.focus) === 'function') {
      event.target.nextSibling.focus();
    }
  }


  dragOver(event: Event) {
    console.log(event.target);
    return false;
  }

  dragEnd(event: Event) {
    console.log('ended!');
  }

  dragDrop(event: Event) {
    console.log('dropped!');
  }
}
