export class Song {
    // tslint:disable: variable-name
    name: string;
    artist: string;
    album: string;
    url: string;
    cover_art_url: string;
    lyrics: string;
    playlist_index: number;
    rng_index: number;
}
