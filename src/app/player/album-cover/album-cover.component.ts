import { Component, OnInit, Input } from '@angular/core';
import { Song } from '../song/song.model';

@Component({
  selector: 'app-album-cover',
  templateUrl: './album-cover.component.html',
  styleUrls: ['./album-cover.component.scss']
})
export class AlbumCoverComponent {

  constructor() { }

  @Input() song: Song;
  @Input() flippable = true;
  flippedFlag = false;

  flip() {
    if (this.flippable) {
      this.flippedFlag = !this.flippedFlag;
    }
  }
}
