import { Song } from '../song/song.model';

export class Playlist {
    songs: Song[];
    title: string;
    author: string;
}
