import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { AudioService } from 'src/app/audio/audio.service';
import { AudioInfo } from '../../shared/playtime/audioinfo.model';
import { timer, Subscription } from 'rxjs';

@Component({
  selector: 'app-timebar',
  templateUrl: './timebar.component.html',
  styleUrls: ['./timebar.component.scss']
})
export class TimebarComponent implements OnInit, OnDestroy {

  constructor(
    public readonly audioService: AudioService,
    private cd: ChangeDetectorRef
    ) { }

  subscriptions = new Subscription();

  ngOnInit() {
    this.initializeTimer();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  initializeTimer() {
    this.cd.detach();
    const updater = timer(0, 200);
    this.subscriptions.add(updater.subscribe(() => this.update()));
  }

  public get info(): AudioInfo {
    return {
      progress: this.audioService.progress,
      duration: this.audioService.duration
    };
  }

  public set info(value: AudioInfo) {
    this.audioService.progress = value.progress;
  }

  public update() {
    this.cd.detectChanges();
  }
}
