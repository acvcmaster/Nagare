export class AudioInfo {
    progress: number;
    duration: number;
}
