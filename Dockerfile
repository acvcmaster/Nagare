FROM nginx:latest

COPY dist/Nagare Nagare

# set environment variables
ENV BUILD Nagare

# add to nginx
RUN rm -r /usr/share/nginx/html
RUN cp -r ${BUILD} /usr/share/nginx/html
RUN cp -r ${BUILD}/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80/TCP